﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ParcialDosDiego
{
	public partial class App : Application
	{
        public static String urlBd;
        public App ()
		{
			InitializeComponent();

			MainPage = new ParcialDosDiego.MainPage();
		}

        // creamos la nueva

        public App(String url)
        {
            InitializeComponent();

            urlBd = url;

            MainPage = new ParcialDosDiego.MainPage();
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
